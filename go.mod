module gitlab.com/lechengbao

go 1.20

require (
	github.com/go-redis/redis/v7 v7.4.1
	github.com/shopspring/decimal v1.3.1
)
