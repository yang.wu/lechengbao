package main

import (
	"encoding/json"
	"fmt"
	"math/rand"

	"github.com/shopspring/decimal"
	"gitlab.com/lechengbao/pkg/public/conn"
	"gitlab.com/lechengbao/pkg/service"
)

func init() {
	conn.RS()
}

func main() {
	timestamp := 1704873581

	for i := 1; i <= 100000; i++ {
		score := rand.Intn(10001)
		service.AddUserToRanking(i, score, int64(timestamp+i))
	}

	// 排行榜
	ranks := service.RankingListWithSortSet("202401", 0, 20)
	b, _ := json.Marshal(ranks)
	fmt.Println(string(b))

	// 用户 id 为 2 的用户前后 10 名玩家的分数跟名次
	ret := service.GetUserRank("202401", 2, 10)
	b, _ = json.Marshal(ret)
	fmt.Println(string(b))
}

// 如果玩家分数，触发时间均相同，则根据玩家等级排名。
// level 用户等级(1-100)
// 名字同理，按照需求如何排名设计。
func getUserRankScore(score int, level int, createdAt int64) float64 {
	scoreF, _ := decimal.NewFromInt(int64(score)).Add(decimal.NewFromInt(2147483647).Add(decimal.NewFromInt(-createdAt)).Mul(decimal.NewFromFloat(0.000000001))).Add(decimal.NewFromFloat(0.000000000001 * float64(level))).Float64()
	return scoreF
}
