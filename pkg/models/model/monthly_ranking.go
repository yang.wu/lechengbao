package model

import "fmt"

const (
	monthlyRanking = "monthly_ranking_%s" // SortSet 月活动排行榜，后拼接年月。
)

// 获取活动排行榜系统缓存key
// month 月份，格式-202401
func GetMonthlyRankingKey(month string) string {
	return fmt.Sprintf(monthlyRanking, month)
}

type UserRanking struct {
	UserID int   `json:"user_id"` // 用户 ID
	Rank   int64 `json:"rank"`    // 排名
	Score  int   `json:"score"`   // 分数
}

type UserRankVO struct {
	Rows []UserRanking `json:"rows"`
}
