package service

import (
	"fmt"
	"strconv"
	"time"

	"github.com/go-redis/redis/v7"
	"github.com/shopspring/decimal"
	"gitlab.com/lechengbao/pkg/models/model"
	"gitlab.com/lechengbao/pkg/public/conn"
)

// 将用户添加到排行榜
// userID 用户ID，score 用户分数，createdAt 创建时间
func AddUserToRanking(userID, score int, createdAt int64) error {
	createdTime := time.Unix(createdAt, 0)
	month := createdTime.Format("200601")
	key := model.GetMonthlyRankingKey(month)
	return conn.RS().ZAdd(key, &redis.Z{Score: getUserRankScore(score, createdAt), Member: userID}).Err()
}

// 获取用户加入排行的Score
func getUserRankScore(score int, createdAt int64) float64 {
	scoreF, _ := decimal.NewFromInt(int64(score)).Add(decimal.NewFromInt(2147483647).Add(decimal.NewFromInt(-createdAt)).Mul(decimal.NewFromFloat(0.000000001))).Float64()
	return scoreF
}

// 根据用户ID获取用户排行榜的分数，返回 -1 则不在排行榜
func GetUserRankScoreByUserID(month string, userID int) float64 {
	key := model.GetMonthlyRankingKey(month)
	score, err := conn.RS().ZScore(key, strconv.Itoa(userID)).Result()
	if err != nil {
		return -1
	}
	return score
}

// 获取排名列表
func RankingListWithSortSet(month string, lastID, size int) []model.UserRanking {
	key := model.GetMonthlyRankingKey(month)

	var score float64
	var firstRank int64
	if lastID != 0 {
		score = GetUserRankScoreByUserID(month, lastID)
		if score == -1 {
			return make([]model.UserRanking, 0)
		}
		firstRank, _ = conn.RS().ZRevRank(key, strconv.Itoa(lastID)).Result()
		firstRank++
	}

	// 正序使用ZRangeByScore, 倒序使用ZRevRangeByScore
	var cacheData []redis.Z
	var err error

	var max string
	if lastID == 0 {
		max = "+inf"
	} else {
		max = "(" + fmt.Sprintf("%v", score)
	}
	cacheData, err = conn.RS().ZRevRangeByScoreWithScores(key, &redis.ZRangeBy{Min: "-inf", Max: max, Offset: 0, Count: int64(size)}).Result()

	if err != nil {
		return nil
	}

	var userRankingList []model.UserRanking
	for _, v := range cacheData {
		firstRank++
		userIDStr, ok := v.Member.(string)
		if !ok {
			continue
		}
		userID, err := strconv.Atoi(userIDStr)
		if err != nil {
			continue
		}
		userRankingList = append(userRankingList, model.UserRanking{
			UserID: userID,
			Rank:   firstRank,
			Score:  int(v.Score),
		})
	}

	return userRankingList
}

// 获取用户前后几位玩家的排行信息
func GetUserRank(month string, userID, size int) model.UserRankVO {
	ret := model.UserRankVO{
		Rows: []model.UserRanking{},
	}
	key := model.GetMonthlyRankingKey(month)
	firstRank, err := conn.RS().ZRevRank(key, strconv.Itoa(userID)).Result()
	if err != nil {
		return ret
	}
	var start, stop int64
	start = firstRank - int64(size)
	if start < 0 {
		start = 0
	}
	stop = firstRank + int64(size)
	cacheData, err := conn.RS().ZRevRangeWithScores(key, start, stop).Result()
	if err != nil {
		return ret
	}
	for k, v := range cacheData {
		var userID int
		userIDStr, ok := v.Member.(string)
		if ok {
			userID, _ = strconv.Atoi(userIDStr)
		}
		ret.Rows = append(ret.Rows, model.UserRanking{
			UserID: userID,
			Rank:   firstRank + int64(k),
			Score:  int(v.Score),
		})
	}
	return ret
}
