package conn

import (
	"sync"

	"github.com/go-redis/redis/v7"
)

var _RS *redis.Client
var rsOnce sync.Once

func RS() *redis.Client {
	rsOnce.Do(func() {
		initRedisConn(0)
	})
	return _RS
}

func initRedisConn(index int) {
	// 创建 Redis 客户端
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379", // Redis 服务器地址及端口号
		Password: "",               // Redis 密码（若无则为空字符串）
		DB:       index,            // 选择数据库索引（默认为0）
	})

	// Ping 测试连接状态
	_, err := client.Ping().Result()
	if err != nil {
		panic(err)
	}
	_RS = client
}
